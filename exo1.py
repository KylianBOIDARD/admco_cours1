#!/usr/bin/env python
# coding "utf-8"

def divide(a,b):
    return a/b

def multiplier(a,b):
    return a*b

def add(a,b):
    return a+b

def minus(a,b):
    return a-b


############### TEST DES FONCTIONS ###################

a=10
b=2
print(divide(a,b))
print(multiplier(a,b))
print(add(a,b))
print(minus(a,b))

