#!/usr/bin/env python
# coding "utf-8"


class SimpleCalculator:
    def __init__(self, nbr1, nbr2):
        self.nbr1 = nbr1
        self.nbr2 = nbr2

    def divide(self, nbr1, nbr2):
        return nbr1 / nbr2

    def multiplier(self, nbr1, nbr2):
        return nbr1 * nbr2

    def add(self, nbr1, nbr2):
        return nbr1 + nbr2

    def minus(self, nbr1, nbr2):
        return nbr1 - nbr2


############### TEST DES FONCTIONS ###################

a = 10
b = 2
Calcul = SimpleCalculator(a, b)
print("a*b=", Calcul.multiplier(a, b))
print("a*b=", Calcul.divide(a, b))
print("a*b=", Calcul.add(a, b))
print("a*b=", Calcul.minus(a, b))
