""" This file is compose by several test class to check the proper
    functionning of the SimpleCalculator class
"""

import unittest
from calculator_pkg.calculator import SimpleCalculator

############### TEST DES FONCTIONS ###################
"""
a = 10
b = 2
#Calcul = SimpleCalculator()
print("a*b=", Calcul.multiplier(5, 2))
print("a*b=", Calcul.divide(a, b))
print("a*b=", Calcul.add(5, 2))
print("a*b=", Calcul.minus(a, b))
"""
####### CLASSE DE TEST AVEC UNNITEST #################


class TestMultiplier(unittest.TestCase):
    """
    This is a test class for multiply.
    """

    def setUp(self):
        """
        The function to setup the class.
        """
        self.calculator = SimpleCalculator()

    def test_multiplier_float(self):
        """
        The function to test if the result of multipy is correct
        if the number are float
        """
        self.assertEqual(self.calculator.multiplier(1.2, 2), "Error")

    def test_multiplier_int(self):
        """
        The function to test if the result of multipy is correct
        """
        result = self.calculator.multiplier(2, 2)
        self.assertEqual(result, 4)


class TestDivide(unittest.TestCase):
    """
    This is a test class for multiply.
    """

    def setUp(self):
        """
        The function to setup the class.
        """
        self.calculator = SimpleCalculator()

    def test_divideby0(self):
        """
        The function to test if the result of division by 0 is correct
        """
        result = self.calculator.divide(2, 0)
        self.assertEqual(result, "Error divide by 0")

    def test_divide_float(self):
        """
        The function to test if the result of division by float is correct
        """
        result = self.calculator.divide(2, 1.5)
        self.assertEqual(result, "Error")


class TestAdd(unittest.TestCase):
    """
    This is a test class for addition.
    """
    def setUp(self):
        """
        The function to setup the class.
        """
        self.calculator = SimpleCalculator()

    def test_add_float(self):
        """
        The function to test if the result of addition with float is correct
        """
        self.assertEqual(self.calculator.add(3.0, 1), "Error")


###### TO DO LIST #################
"""
- create test class for Minus
- add other test function to be sure we will test every case
"""


if __name__ == "__main__":
    unittest.main()
