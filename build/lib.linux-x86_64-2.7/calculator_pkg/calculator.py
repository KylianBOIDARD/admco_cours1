#!/usr/bin/env python
# coding: "utf-8"

""" This class is use to do operation on two int numbers """

import logging


class SimpleCalculator():
    """
    This is a class for mathematical operations.
    """
    def __init__(self):
        return

    def divide(self, nbr1, nbr2):
        """
        The function to divide two Numbers.

        Parameters:
            nbr1 (int)
            nbr2 (int)

        Returns:
            "Error" if nbr1 or nbr2 is not an int
            "Error" if nbr2 is 0
            Division of nbr1 / nbr2

        """
        if isinstance(nbr1, int) & isinstance(nbr2, int):  # check if nbr1 and nbr2 are type int
            if nbr2 == 0:  # check nbr2 is 0
                logging.error("Try to divide by 0")
                return "Error divide by 0"
            return nbr1 / nbr2
        logging.error("Try to divide by float")
        return "Error"

    def multiplier(self, nbr1, nbr2):
        """
        The function to multiply two Numbers.

        Parameters:
            nbr1 (int)
            nbr2 (int)

        Returns:
            "Error" if nbr1 or nbr2 is not an int
            Addition of nbr1 * nbr2

        """
        if isinstance(nbr1, int) & isinstance(nbr2, int):  # check if nbr1 and nbr2 are type int
            return nbr1 * nbr2
        return "Error"

    def add(self, nbr1, nbr2):
        """
        The function to add two Numbers.

        Parameters:
            nbr1 (int)
            nbr2 (int)

        Returns:
            "Error" if nbr1 or nbr2 is not an int
            Addition of nbr1 + nbr2

        """
        if isinstance(nbr1, int) & isinstance(nbr2, int):  # check if nbr1 and nbr2 are type int
            return nbr1 + nbr2
        return "Error"

    def minus(self, nbr1, nbr2):
        """
        The function to minus two Numbers.

        Parameters:
            nbr1 (int)
            nbr2 (int)

        Returns:
           "Error" if nbr1 or nbr2 is not an int
            Substract of nbr1 - nbr2

        """
        if isinstance(nbr1, int) & isinstance(nbr2, int):  # check if nbr1 and nbr2 are type int
            return nbr1 - nbr2
        return "Error"
