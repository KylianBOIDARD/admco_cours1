""" This file is the setup file
    Use to build PyPi wheel pkg
"""

from setuptools import setup

setup(
    name="SimpleCalculatorKylian",
    version="0.0.1",
    author="Kylian BOIDARD",
    packages=["calculator_pkg"],
    description="SimpleCalculator is a simple programm to calculate",
    license="GNU GPLv3",
    python_requires=">=2.4",
)
